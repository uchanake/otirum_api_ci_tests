# Technical Part-3
### API Documentation: https://documenter.getpostman.com/view/6971845/UVByHpuM

## CI Integrated API Collection contains 3 APIs

1.  ###### Get an Album
1.  ###### Get Several Albums
1.  ###### Get an Album's Tracks

## How to run tests ?
1. Go to one of the Spotify API to get Token for you. (https://developer.spotify.com/console/get-album-tracks/)
2. Click Get Token button to get new token. You will have to sign-up for Spotify if you do not have an account
3. Copy the access token and paste it in the postman collection Authorization section as the Token
4. Clone the project
5. Import json file using postman
6. Select the collection and go to **Authorization** tab, paste the Spotify token there in the **Token**
7. Follow the below steps to execute the tests locally in CLI
8. You can run the test by giving the data file *(albumData.csv)*
8. Export token updated project to run in CLI mode
9. Follow below steps to run in CLI

*Install newman* 
```
npm install -g newman-reporter-htmlextra
```
*Run postman tests in CLI mode*
```
newman run otrium_api_ci_tests.postman_collection.json --iteration-data albumData.csv --reporters htmlextra 
```
10. Push the code to repo and CI job will start

## How to write new test ?
1. Open the *(albumData.csv)*
2. Add your data as per the column variables and save it as *.csv* file
3. Push to repo and CI pipeline will start (or else you try step 9 to run locally)